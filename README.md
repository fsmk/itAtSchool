<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#sec-1">1. Introduction</a></li>
<li><a href="#sec-2">2. Syllabus</a>
<ul>
<li><a href="#sec-2-1">2.1. Getting help</a></li>
<li><a href="#sec-2-2">2.2. File management</a></li>
<li><a href="#sec-2-3">2.3. Users and Groups</a></li>
<li><a href="#sec-2-4">2.4. File Permissions and File attributes</a></li>
<li><a href="#sec-2-5">2.5. Package Management</a></li>
<li><a href="#sec-2-6">2.6. Process Management</a></li>
</ul>
</li>
</ul>
</div>
</div>


# Introduction<a id="sec-1" name="sec-1"></a>

This is a summary of the topics that we have covered till date. Other
topics (extra commands) that we haven't covered in detail but belong
to the same category can also be accessed in this document.

This document, refered to as a syllabus, is a work in progress and
will go through multiple iterations in the near future.

The goal of this document is not to **teach** GNU LINUX (partly
because it takes lot of effort and time to learn GNU LINUX, and
because we ourselves are continuously learning GNU LINUX); the primary
objective of this document is to bridge the psychological barrier that
inhibits most people while learning GNU LINUX.

This document is NOT an exhaustive means of everything that is there
to learn about GNU LINUX. This is meant to assist people to get
started. This document will help any individual to be able to read and
decipher any LINUX related jargon online, and thus ease their journey
of learning.

NOTE:
-   Commands are shown along with some important options
-   Most commands follow the syntax: command-name [-options] [target]
-   The options mentioned here are not an exhaustive list of all the
    options that are present. The idea is that if you need some other
    options then you should be able to find it yourselves.

# Syllabus<a id="sec-2" name="sec-2"></a>

## Getting help<a id="sec-2-1" name="sec-2-1"></a>

-   **man:** This is a command that displays the manual page for each command
    -   **Syntax:** $ man [-options] command-name
    -   Navigating man
        -   The man pages uses vim keybindings
        -   up, down arrow keys work fine
        -   you can use the "/" key to open up the search and use n/N to cycle through the hits
    -   Examples
        -   $ man ls
        -   $ man <any-other-command>

## File management<a id="sec-2-2" name="sec-2-2"></a>

-   **ls:** lists all the files and directories in the current working directory
    -   **Syntax:** $ ls [-options] [location]
    -   Common Options
        -   **-a:** lists all the file and directories in the curernt workign directory
        -   **-l:** lists list the output in long format displaying information like size and blocks and permissions
        -   **-h:** displays the size of the files / directories in human readable format
        -   **-i:** displays the inode number of each file/directory
    -   Examples
        -   $ ls -l /usr/bin
        -   $ ls -h
        -   $ ls -al
-   **pwd:** print working directory; shows the current working directory
    -   **Syntax:** $ pwd [-options]
    -   Common Options
        -   Nothing Commonly used
-   **mkdir:** creates a new directory
    -   **Syntax:** $ mkdir [-options] [directory name]
    -   Common Options
        -   -p : make parent directories as needed
        -   -v : print a message for each directory created
    -   Examples 
        -   $ mkdir testdir
-   **cd:** changes the directory
    -   **Syntax:** $ cd [-options] [target]
    -   Example 
        -   $ cd directory
-   **touch:** Changes file timestamp (used to create new empty files too)
    -   **Syntax:** $ touch [-options] [target]
    -   Common Options
        -   -a : change only the access time
        -   -m : change only the modification time
    -   Examples
        -   $ touch fileName
-   **stat:** display file/filesystem status
    -   **Syntax:** $ stat [-options] [target]
    -   Common Options
        -   **-t:** tearse output
    -   Examples 
        -   $ stat fileName
-   **rm:** removes a file
    -   **Syntax:** $ rm [-options] [target]
    -   Common Options
        -   **-r:** recursively delete all file and directories (including the ucrrent directory) within the target directory also.
        -   **-f:** force option, never prompt the user for input
        -   **-v:** explain what is being done
    -   Examples
        -   $ rm -rf factoryName
        -   $ rm -rf she will also spanish along with her.
-   **mv:** used to move a file form one location to another location
    -   **Syntax:** $ mv [-options] source(s) destination
    -   Common Options
        -   **-f:** force; do not prompt before over-writing
        -   **-i:** interactive; always prompt way to act also i guess
        -   **-u:** UPDATE; move only when the SOURCE file is newer than the DESTINATION file
    -   Examples
        -   $ mv -r /directory/directory1 /newdir/newdir1
        -   $ mv testfile1 testfile2 testfile3 /newdir
-   **cp:** to copy a file from one location to another location
    -   Syntax
        -   $ cp [-options] [SOURCE] [DESTINATION]
    -   Common Options
        -   **-r:** copy directories recursively
        -   **-v:** explain what is being done
    -   Examples
        -   $ cp -r *home/user1/test* *home/test/archive*
-   **cat:** concatenate files and print them in standard output
    -   **Syntax:** $ cat [-options] [fileName(s)]
    -   Common Options
        -   **-n:** Show number of output lines
    -   Examples
        -   \# cat -n testFile1 testFile2 testFile3

## Users and Groups<a id="sec-2-3" name="sec-2-3"></a>

-   **useradd:** Used to add a new user to the system
    -   **Syntax:** \# useradd [-options] [values] newUserName
    -   Common Options 
        -   **-d:** Specify the home directory of the new user
        -   **-s:** Specify user login shell
        -   **-g:** Specify the initial group or Group id when creating a new user
        -   **-G:** Specify user groups
        -   **-u:** Specify user user<sub>id</sub>
    -   Examples
        -   \# useradd -d /home/testUser -s /bin/bash -u 999 -g 249 testUser
-   **usermod:** user to modify the settings of an existing user on the system
    -   **Syntax:** \# usermod [-options] [values] existingUser
    -   Common Options
        -   **-a:** append the user to supplimentary groups (should only be used with -G options)
        -   **-d:** change the users current home directory (if used with the
            -m option then the contents of the home directory are also
            moved)
        -   **-m:** move the contents of the current home directory to the new
            home directory (this option is only valid when used with the
            -d option)
        -   **-g:** the new name or number of the users new initial login group
        -   **-G:** list of all the supplimentary groups the user is also a part of
        -   **-s:** the name of the users default login shell
    -   Examples
        -   \# usermod -d /home/someOtherdir/testUser testUser
-   **userdel:** used to delete a user from the system
    -   Common Options
        -   **-r:** Remove the users home directory and mail spool
-   **groupadd:** adding a new group to the system
    -   Common Options
        -   **-f:** forces the creating of a new group
        -   **-g:** specify the group-id of the new group. This should be unique.
-   **groupmod:** modifying an existing group on the system
    -   Common Options
        -   **-g:** changes the group id of the group to the new group id (this must be a non-negetive integer)
        -   **-n :** new name of the group
-   **groupdel:** deleting an existing group in the system
    -   nothing extraordinalrily special

## File Permissions and File attributes<a id="sec-2-4" name="sec-2-4"></a>

-   **chmod:** change the mode of a file/directory
    -   **Syntax:** $ chmod [options] mode file
    -   Options
        -   **-v:** report a diagonostic for every file processed
        -   **-c:** report for every change that is being made
        -   **-f:** suppress most error messages
        -   **-R:** change file and directories recursively
    -   Examples
        -   $ chmod [options] octal-mode file
        -   $ chmod -R 754 /home/\*
        -   $ chmod u+x ./testfile
-   **chown:** change the owner/group of a file/directory
    -   **Syntax:** $ chown [options] user:group target
    -   Options
        -   **-v:** ouput a diagonostics of everyfile processed
        -   **-c:** report only when a change is made
        -   **-f:** suppress error messages
        -   **-R:** Run recursively
    -   Examples
        -   $ chown -R user1:group1 /home/user1

## Package Management<a id="sec-2-5" name="sec-2-5"></a>

NOTE: This section is very Debian-y. Sure other package managers are there and some are better than dpkg (the Debian package manager) and apt (Advanced packaging tool).The idea is thave even though most package managers are different and do things differently, what they do is fairly similar and thus with the basic idea of how one package manager functions should give you the idea of the whole (not completely!!) arena.
-   **apt-get:** Apt package handling utility (CLI)
    -   **Syntax:** \# apt-get [options] command [pkg]
    -   Commands
        -   **update:** used to re-synchronize the package index files from their sources
        -   **upgrade:** upgrades the installed packages from the repository
        -   **dist-upgrade:** in addition to performing the tasks of upgrade this command intelligently handles changing dependencies with new version of the packages
        -   **install:** install a package from the repository
        -   **remove:** remove a package from the system
    -   Example
        -   \# apt-get update
        -   \# apt-get upgrade
        -   \# apt-get install vim
        -   \# apt-get remove vim
-   **apt-cache search:** searches for an app in the repository
-   **dpkg &#x2013;get-selections:** shows a list of all the apps currently installed on the system
-   **dpkg &#x2013;configure -a:** resets dpkg confiuration to the factory settings
-   **add-apt-repository:** used to add a new repository to the sources.list file or to remove an existing entry from the sources.list file.

## Process Management<a id="sec-2-6" name="sec-2-6"></a>

-   **ps:** shows the processess running on the current system
    -   **Syntax:** ps [options]
    -   Common Options
        -   **-e:** to select every process on the system
        -   **-l:** Long listing format
        -   **-u:** selects processess by effective user id
        -   **-U:** selects processess by real user id
        -   **-f:** Full format listing
    -   Examples
        -   $ ps -ely
        -   $ ps -elf
        -   $ ps -u user1

-   **pstree:** Display a tree of the current processess
    -   **Syntax:** $ pstree [options] [pid]
    -   Common Options
        -   **-p:** show pids
    -   Example
        -   $ pstree
        -   $ pstree 241323
        -   **-l:** dislpay long lines
-   **kill:** Kill a process by pid
-   **killall:** Kill a process by name
-   **pkill:** Send signals to a specific process
